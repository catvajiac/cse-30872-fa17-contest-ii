#!/usr/bin/env python2.7

import sys

def num_hops(x, y):
  step = 1
  #return 1 + num_hops_r(x+1, y-1, 1, 1)
  print num_hops_r(x+1, y, 1, 1)

def num_hops_r(x, y, step, count):
  print x, y, step, count
  if x == y:
    return count

  count += 1

  left = num_hops_r(x+step, y, step-1, 0)
  mid = num_hops_r(x+step, y, step, 0)
  right = num_hops_r(x+step, y, step+1, 0)

  if left < mid and left < right:
    return left
  if mid < left and mid < right:
    return mid
  else:
    return right

  return count

if __name__ == "__main__":

  for line in sys.stdin:
    x, y = sorted(map(int, line.split()))

    print "{} -> {} takes {} hops".format(x, y, num_hops(x, y))
  
