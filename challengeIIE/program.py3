#!/usr/bin/python3

import sys, math

pattern = [0]
for i in range(9999):
  t = math.ceil(i/2)
  for j in range(t):
    pattern.append(i)

for line in sys.stdin:
  x,y = map(int, line.strip().split())
  if x > y:
    diff = x-y
  else:
    diff = y-x

  print("{} -> {} takes {} hops".format(x, y, pattern[diff]))

  #1 =1
  #2 =2

  #3 =3
  #4 =3
  #5 =4
  #6 =4
  
  #7 =5
  #8 =5
  #9 =5
  #10 =6
  #11 =6
  #12 =6
