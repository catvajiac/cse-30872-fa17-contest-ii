#!/usr/bin/env python2.7

import sys

if __name__ == "__main__":
  num_tacos = sys.stdin.readline()

  while num_tacos:

    tacos = map(int, sys.stdin.readline().split())
  
    sum = 0
    for i, taco in enumerate(sorted(tacos, reverse=True)):
      sum += 2**i * taco
  
    print sum
    num_tacos = sys.stdin.readline()
  
