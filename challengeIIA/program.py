#!/usr/bin/env python2.7

import sys

def read_graph(n, n_edges):
  g = {v: set() for v in range(1, n+1)}

  for _ in range(n_edges):
    s, t = map(int, sys.stdin.readline().split())

    g[s].add(t)
    g[t].add(s)

  return g

def find_components(g):
  visited = set()
  components = list()

  for node in g:
    component = []
    find_components_r(g, node, visited, component)
    if len(component):
      components.append(component)

  return components

def find_components_r(g, source, visited, component):
  if source in visited:
    return
  
  visited.add(source)
  component.append(source)

  for neighbor in g[source]:
    find_components_r(g, neighbor, visited, component)
    

if __name__ == "__main__":
  n_nodes = sys.stdin.readline()
  i = 0

  while n_nodes:
    i += 1
    n_nodes = int(n_nodes)
    n_edges = int(sys.stdin.readline())

    g = read_graph(n_nodes, n_edges)

    components = sorted(map(sorted, find_components(g)))
    
    print "Graph {} has {} groups:".format(i, len(components))
    for component in components:
      print " ".join(map(str, component))

    n_nodes = sys.stdin.readline()
