#!/usr/bin/python

import sys
import itertools

if __name__ == "__main__":

  while True:
    a = sys.stdin.readline().strip()
    if not a:
      break
    b = sys.stdin.readline().strip()

    alist = []
    blist = []
    for c in a:
      alist.append(c)
    for c in b:
      blist.append(c)

 
    letters = []

    if len(alist) < len(blist):
      for c in alist:
        if c in blist:
          letters.append(c)
    else:
      for c in blist:
        if c in alist:
          letters.append(c)
  
    l = ''.join(sorted(letters))
    print(l)
