#!/usr/bin/env python2.7

import sys

def min_weight(matrix):
  nrows = len(matrix)
  ncols = len(matrix[0])
  weights = [[99999 for y in range(ncols)] for x in range(nrows)]
  paths = [[0 for y in range(ncols)] for x in range(nrows)]
  start = [x[0] for x in matrix]

  for i, s in enumerate(start):
    weights[i][0] = s
    paths[i][0] = i

  for row, cell in enumerate(start):
    min_weight_r(matrix, weights, paths, row, 0)

  return recreate_path(weights, paths)

def min_weight_r(matrix, weights, paths, row, col):
  if col + 1 == len(matrix[0]):
    return

  up = row - 1 if row - 1 >= 0 else len(matrix) - 1
  down = row + 1 if row + 1 < len(matrix) else 0

  for r in [up, row, down]:
    if weights[row][col] + matrix[r][col+1] < weights[r][col+1]:
      weights[r][col+1] = weights[row][col] + matrix[r][col+1]
      paths[r][col+1] = row

    if col < len(matrix[0]):
      min_weight_r(matrix, weights, paths, r, col+1)

def recreate_path(weights, paths):
  col = len(paths[0]) - 1
  cols = [x[col] for x in weights]
  row = cols.index(min(cols))
  lst = []
  lst.append(row+1)
  for i in reversed(range(1, len(paths[0]))):
    row = int(paths[row][i])
    lst.append(paths[row][i] + 1)

  lst.reverse()
  return min(cols), map(int, lst)

if __name__ == "__main__":
  line = sys.stdin.readline()

  while line:
    m, n = map(int, line.split())
    matrix = []

    for _ in range(m):
      line = map(int, sys.stdin.readline().split())
      matrix.append(line)

    weight, rows = min_weight(matrix)
    print weight
    print " ".join(map(str, rows))

    line = sys.stdin.readline()
