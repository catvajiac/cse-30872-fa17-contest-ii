import sys

def isMutant(w1,w2):
  w1 = str(w1.lower())
  w2 = str(w2.lower())

  # w1 is always longest
  if len(w1) < len(w2):
    w1, w2 = w2, w1

  editted = False
  while len(w1) > 0 and len(w2) > 0:
    if w1[-1] == w2[-1]:
      w1 = w1[:-1]
      w2 = w2[:-1]
    #w1 is longest, it would have the extra char in an insert/delete
    else:
      if editted:
        return False
      editted = True
      w1 = w1[:-1]
      if len(w1) != len(w2):
        w2 = w2[:-1]
  
  return True

def mutationPath(lastWord, words):
  md = 0
  mp = []
  mi = len(words) - 1
  temp = list(words)
  while len(temp) > 0:
    tw = temp.pop(0)
    if not isMutant(lastWord, tw):
      continue
    p = mutationPath(tw, temp)
    d = len(p) + 1
    if d > md:
      md = d
      mp = list(p)
      mp.append(tw)
  return mp

if __name__ == "__main__":
  words = list()
  for line in sys.stdin:
    if line.isspace(): continue
    words.append(line.strip())

  md = 0
  mp = []
  mi = len(words) - 1
  while len(words) > 0:
    tw = words.pop(0)
    p = mutationPath(tw, words)
    d = len(p) + 1
    if d > md:
      md = d
      mp = list(p)
      mp.append(tw)
  
  print md
  mp.reverse()
  for w in mp:
    print w
